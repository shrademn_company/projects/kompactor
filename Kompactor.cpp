//------------------------------------------------------------------------------
//
// Kompactor.cpp created by Yyhrs 2020/10/24
//
//------------------------------------------------------------------------------

#include <QMap>
#include <QSet>

#include "Kompactor.hpp"

Kompactor::Kompactor(QFileInfo const &input, QFileInfo const &output, QStringList const &shaders, const Kompactor::Precisions &precisions):
	m_input{input},
	m_output{output},
	m_shaders{shaders},
	m_precisions{precisions}
{
}

QList<Kompactor::Report> Kompactor::execute()
{
	QList<Report> reports;
	Alamo::Model  model{m_input};

	for (auto &mesh: model.meshes())
		for (auto &subMesh: mesh.subMeshes)
			if (m_shaders.contains(subMesh.shader))
			{
				auto oldVerticesSize{subMesh.vertices.size()};
				auto oldIndicesSize{subMesh.indices.size()};

				rebuildMesh(subMesh);
				reports << Report{mesh.name, oldVerticesSize, subMesh.vertices.size(), oldIndicesSize / 3, subMesh.indices.size() / 3};
			}
	model.save(m_output.absoluteFilePath());
	if (!model.error().isNull())
		reports << Report{m_input.fileName(), 0, 0, 0, 0};
	return reports;
}

void Kompactor::rebuildMesh(Alamo::Model::SubMesh &subMesh)
{
	QMap<Key, uint16_t>           keyIndices;
	QMap<uint16_t, uint16_t>      newIndices;
	uint16_t                      count{0};
	QVector<Alamo::MASTER_VERTEX> vertices;
	QVector<uint16_t>             indices;

	vertices.reserve(subMesh.vertices.size());
	for (int index{0}; index < subMesh.vertices.size(); ++index)
	{
		auto const &vertex{subMesh.vertices[index]};
		Key const  key{QString::number(vertex.position.x(), 'f', m_precisions.vertex),
					   QString::number(vertex.position.y(), 'f', m_precisions.vertex),
					   QString::number(vertex.position.z(), 'f', m_precisions.vertex),
					   QString::number(vertex.normal.x(), 'f', m_precisions.normal),
					   QString::number(vertex.normal.y(), 'f', m_precisions.normal),
					   QString::number(vertex.normal.z(), 'f', m_precisions.normal),
					   m_precisions.takeUv ? QString::number(vertex.texCoord[0].x(), 'f', m_precisions.normal) : "",
					   m_precisions.takeUv ? QString::number(vertex.texCoord[0].y(), 'f', m_precisions.normal) : ""};

		if (keyIndices.contains(key))
			newIndices[index] = keyIndices[key];
		else
		{
			newIndices[index] = count;
			keyIndices[key] = count;
			vertices.append(vertex);
			++count;
		}
	}
	indices.reserve(subMesh.indices.size());
	for (int index{0}; index < subMesh.indices.size(); index = index + 3)
	{
		Face face{newIndices.value(subMesh.indices[index]), newIndices.value(subMesh.indices[index + 1]), newIndices.value(subMesh.indices[index + 2])};

		if (std::get<0>(face) != std::get<1>(face) && std::get<0>(face) != std::get<2>(face) && std::get<1>(face) != std::get<2>(face))
		{
			indices.append(std::get<0>(face));
			indices.append(std::get<1>(face));
			indices.append(std::get<2>(face));
		}
	}
	subMesh.vertices = vertices;
	subMesh.indices = indices;
}

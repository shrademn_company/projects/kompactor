//------------------------------------------------------------------------------
//
// MainWindow.cpp created by Yyhrs 2020-10-23
//
//------------------------------------------------------------------------------

#include <QFileDialog>
#include <QtConcurrent>

#include <core.tpp>
#include <operators.hpp>
#include <SApplication.hpp>

#include "Kompactor.hpp"
#include "MainWindow.hpp"

MainWindow::MainWindow(QWidget *parent):
	QMainWindow{parent}
{
	QVector<QPair<QString, int>> precisions{
		{"FULL", -1},
		{"9", 9},
		{"8", 8},
		{"7", 7},
		{"6", 6},
		{"5", 5},
		{"4", 4},
		{"3", 3},
		{"2", 2},
		{"1", 1},
		{"GGGWARRRHHWWWW", 0}};

	sApp->setTheme(m_settings.value(SApplication::Theme, Theme::c_dark).toString());
	sApp->startSplashScreen(Logos::c_shrademnCompanyRed.arg(Theme::c_light));
	setupUi(this);
	for (auto const &precision: precisions)
	{
		vertexPrecisionComboBox->addItem(precision.first, precision.second);
		normalPrecisionComboBox->addItem(precision.first, precision.second);
		uvPrecisionComboBox->addItem(precision.first, precision.second);
	}
	movieLabel->setMovie(&m_3263827);
	reportWidget->addColumns(QList{File, OldFileSize, NewFileSize, MeshName, OldVertexNumber, NewVertexNumber, OldFaceNumber, NewFaceNumber});
	reportWidget->hideColumn(LogWidget::Time);
	reportWidget->hideColumn(LogWidget::Log);
	connectActions();
	connectWidgets();
	m_settings.restoreState(this);
	inputPathLineEdit->setText(m_settings.value(InputPath).toString());
	outputPathLineEdit->setText(m_settings.value(OutputPath).toString());
	m_settings.restoreValues(Shaders, listWidget, {"MeshShadowVolume.fxo"});
	vertexPrecisionComboBox->setCurrentIndex(m_settings.value(VertexPrecision, vertexPrecisionComboBox->currentIndex()).toInt());
	normalPrecisionComboBox->setCurrentIndex(m_settings.value(NormalPrecision, normalPrecisionComboBox->currentIndex()).toInt());
	uvPrecisionCheckBox->setChecked(m_settings.value(TakeUVPrecision).toBool());
	uvPrecisionComboBox->setCurrentIndex(m_settings.value(UVPrecision, uvPrecisionComboBox->currentIndex()).toInt());
	sApp->stopSplashScreen(this);
}

MainWindow::~MainWindow()
{
	m_settings.saveState(this);
	m_settings.setValue(SApplication::Theme, QIcon::themeName());
	m_settings.setValue(InputPath, inputPathLineEdit->text());
	m_settings.setValue(OutputPath, outputPathLineEdit->text());
	m_settings.saveValues(Shaders, listWidget);
	m_settings.setValue(VertexPrecision, vertexPrecisionComboBox->currentIndex());
	m_settings.setValue(NormalPrecision, normalPrecisionComboBox->currentIndex());
	m_settings.setValue(TakeUVPrecision, uvPrecisionCheckBox->isChecked());
	m_settings.setValue(UVPrecision, uvPrecisionComboBox->currentIndex());
}

void MainWindow::connectActions()
{
	listWidget->setOpenOption(PathListWidget::File, "Shader (*.fxo)");
	addToolButton->setDefaultAction(listWidget->actions()[PathListWidget::Add]);
	removeToolButton->setDefaultAction(listWidget->actions()[PathListWidget::Remove]);
	addAction(actionCycleTheme);
	connect(actionCycleTheme, &QAction::triggered, this, &SApplication::cycleTheme);
}

void MainWindow::connectWidgets()
{
	connect(inputPushButton, &QPushButton::clicked, this, [this]
	{
		inputPathLineEdit->openBrowser(PathLineEdit::Folder);
	});
	connect(inputPathLineEdit, &PathLineEdit::textChanged, this, [this](QString const &text)
	{
		progressBar->setDisabled(text.isEmpty() || QFileInfo{text} == QFileInfo{outputPathLineEdit->text()});
	});
	connect(outputPushButton, &QPushButton::clicked, this, [this]
	{
		outputPathLineEdit->openBrowser(PathLineEdit::Folder);
	});
	connect(outputPathLineEdit, &PathLineEdit::textChanged, this, [this](QString const &text)
	{
		progressBar->setDisabled(text.isEmpty() || QFileInfo{text} == QFileInfo{inputPathLineEdit->text()});
	});
	connect(progressBar, &ProgressBarButton::clicked, this, [this]
	{
		QStringList           shaders;
		Kompactor::Precisions precisions{vertexPrecisionComboBox->currentData().toInt(),
										 normalPrecisionComboBox->currentData().toInt(),
										 uvPrecisionCheckBox->isChecked(),
										 uvPrecisionComboBox->currentData().toInt()};

		progressBar->setFormat("%v / %m");
		for (int index = 0; index < listWidget->count(); ++index)
		{
			QFileInfo shader{listWidget->item(index)->text()};

			if (shader.suffix() == "fxo")
				shaders << shader.baseName() + ".fx";
		}
		settingsGroupBox->setDisabled(true);
		stackedWidget->setCurrentIndex(0);
		m_3263827.start();
		reportWidget->clear();
		m_files = File::getFiles(inputPathLineEdit->text(), {"*.alo"});
		QDir{}.mkpath(QFileInfo{outputPathLineEdit->text()}.absoluteFilePath());
		m_watcher.setFuture(QtConcurrent::mapped(m_files, std::function<Result(QFileInfo const &)>{[this, shaders, precisions](QFileInfo const &file)
			{
				Kompactor kompactor{file, outputPathLineEdit->text() / file.fileName(), shaders, precisions};

				return Result{file, kompactor.execute()};
			}}));
	});
	connect(&m_watcher, &QFutureWatcherBase::progressRangeChanged, progressBar, &ProgressBarButton::setRange);
	connect(&m_watcher, &QFutureWatcherBase::progressValueChanged, progressBar, &ProgressBarButton::setValue);
	connect(&m_watcher, &QFutureWatcherBase::resultReadyAt, this, [this](int resultIndex)
	{
		auto const          result{m_watcher.resultAt(resultIndex)};
		QFileInfo const     newFile{outputPathLineEdit->text() / result.first.fileName()};
		QList<QVariantList> descriptions;

		for (auto const &report: result.second)
		{
			QVariantList description;

			description << result.first.baseName()
						<< result.first.size() / 1000000.
						<< newFile.size() / 1000000.
						<< report.name
						<< report.oldVerticesNumber
						<< report.newVerticesNumber
						<< report.oldFaceNumber
						<< report.newFaceNumber;
			if (report.name == result.first.fileName())
				reportWidget->addLog(LogWidget::Warning, result.first.baseName(), description);
			else
				descriptions << description;
		}
		reportWidget->addLogs(LogWidget::Information, result.first.baseName(), descriptions);
	});
	connect(&m_watcher, &QFutureWatcherBase::finished, this, [this]
	{
		settingsGroupBox->setDisabled(false);
		reportWidget->resizeColumnsToContents();
		stackedWidget->setCurrentIndex(1);
		m_3263827.stop();
		progressBar->resetProgressBar();
	});
}

//------------------------------------------------------------------------------
//
// Kompactor.hpp created by Yyhrs 2020/10/24
//
//------------------------------------------------------------------------------

#ifndef KOMPACTOR_HPP
#define KOMPACTOR_HPP

#include <QFileInfo>

#include <gameTypes.hpp>
#include <Models.hpp>

class Kompactor
{
public:
	struct Precisions
	{
		int  vertex{6};
		int  normal{3};
		bool takeUv{true};
		int  uv{6};
	};
	struct Report
	{
		QString   name;
		qsizetype oldVerticesNumber;
		qsizetype newVerticesNumber;
		qsizetype oldFaceNumber;
		qsizetype newFaceNumber;
	};

	Kompactor(QFileInfo const &input, QFileInfo const &output, QStringList const &shaders, Precisions const &precisions);
	~Kompactor() = default;

	QList<Report> execute();

private:
	void rebuildMesh(Alamo::Model::SubMesh &subMesh);

	using Key = std::tuple<QString, QString, QString, QString, QString, QString, QString, QString>;
	using Face = std::tuple<uint16_t, uint16_t, uint16_t>;

	QFileInfo   m_input;
	QFileInfo   m_output;
	QStringList m_shaders;
	Precisions  m_precisions;
};

#endif // KOMPACTOR_HPP

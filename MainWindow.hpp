//------------------------------------------------------------------------------
//
// MainWindow.hpp created by Yyhrs 2020-10-23
//
//------------------------------------------------------------------------------

#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QFileInfo>
#include <QFutureWatcher>
#include <QMovie>

#include <Settings.hpp>

#include "Kompactor.hpp"

#include "ui_MainWindow.h"

class MainWindow: public QMainWindow, private Ui::MainWindow
{
	Q_OBJECT

public:
	enum ReportColumn
	{
		File,
		OldFileSize,
		NewFileSize,
		MeshName,
		OldVertexNumber,
		NewVertexNumber,
		OldFaceNumber,
		NewFaceNumber,
	};
	Q_ENUM(ReportColumn)
	enum SettingsKey
	{
		InputPath,
		OutputPath,
		Shaders,
		VertexPrecision,
		NormalPrecision,
		TakeUVPrecision,
		UVPrecision
	};
	Q_ENUM(SettingsKey)

	explicit MainWindow(QWidget *parent = nullptr);
	~MainWindow();

private:
	void connectActions();
	void connectWidgets();

	using Result = QPair<QFileInfo, QList<Kompactor::Report>>;

	Settings               m_settings;
	QMovie                 m_3263827{":/3263827.gif"};
	QFutureWatcher<Result> m_watcher;
	QFileInfoList          m_files;
};

#endif // MAINWINDOW_HPP
